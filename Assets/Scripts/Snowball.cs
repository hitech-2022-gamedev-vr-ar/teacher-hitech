using System;
using System.Collections;
using UnityEngine;


[RequireComponent(typeof(MeshRenderer), typeof(AudioController))]
public class Snowball : MonoBehaviour
{
    [SerializeField]
    private AudioController audioController;
    private MeshRenderer meshRenderer;
    [SerializeField]
    private ParticleSystem explosionEffect;

    private void Start()
    {
        audioController = GetComponent<AudioController>();
        audioController.PlayShootSound();
    
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(DestroyWithExplosion());
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(DestroyWithExplosion());
    }

    private IEnumerator DestroyWithExplosion()
    {
        var effect = Instantiate(explosionEffect.gameObject, transform.position, Quaternion.identity);
        ParticleSystem part = effect.GetComponent<ParticleSystem>();
        part.Play();
        Destroy(effect, part.main.duration);

        meshRenderer.enabled = false;
        audioController.PlayExplosionSound();
        yield return new WaitWhile(() => audioController.IsPlaying);
        DestroyImmediate(gameObject);
    }
}
