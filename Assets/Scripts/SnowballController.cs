using UnityEngine;

public class SnowballController : MonoBehaviour
{
    [SerializeField] private Rigidbody snowBallPrefab;
    [SerializeField] private Transform hand;
    [SerializeField] private float speed = 1f;

    [SerializeField] private float step = 1f;
    private float nextShoot;

    [SerializeField] private LayerMask targetMask;
    [SerializeField] private float distance = 10f;


    private void Update()
    {
        if (Physics.Raycast(hand.position,
                hand.up, out RaycastHit hit, distance,
                targetMask)
            && Time.time > nextShoot)
        {
            Debug.DrawRay(hand.position, hand.up, 
                Color.red, distance);
            Shoot();
            nextShoot = Time.time + step;
        }
    }

    private void Shoot()
    {
        var snowball = Instantiate(snowBallPrefab,
            hand.position, hand.rotation);
        snowball.AddForce(snowball.transform.up
                          * speed, ForceMode.Impulse);

    }
}
